# Description
This is houses the docker file that creates the image with all the necessary contents
for the LHC-DMWG April 2020 workshop.

# Authors
Sam Meehan <samuel.meehan@cern.ch>

# Stuff Included Here
The following versions and packages are included in the docker image stored in the [gitlab registry](https://gitlab.cern.ch/meehan/docker-lhcdmwg-april2020/container_registry).

## MadGraph5 
Version : v2.7.2

Along with MG5, the following non standard models have been included in the `models` directory :
 - [DMSimp](http://feynrules.irmp.ucl.ac.be/wiki/DMsimp) : The V/AV benchmark model
 - [t-Channel](http://feynrules.irmp.ucl.ac.be/wiki/DMsimpt) : The t-channel model under consideration

## Python
Version : 2.7

Though python3 should be standard, MadGraph seems to not like it.  If you look at the
[Dockerfile](Dockerfile) you will be able to see the necessary directions to install
python3 if you so choose

## pythia8
Version : 8.3.0.1

Pythia 8.2, which is the one that comes when you try to `install` it in MadGraph, has been
deleted from that area.

## LHAPDF 
Version : 6.2.3

You will need to `wget` different versions of the PDF sets using commands like the following
```
wget http://lhapdfsets.web.cern.ch/lhapdfsets/current/CT10nlo.tar.gz -O- | tar xz -C /LHC-DMWG/LHAPDF-Install/share/LHAPDF
```

## HepMC  
Version : 2.06.10

The [CMake directions](http://hepmc.web.cern.ch/hepmc/building.html) for HepMCv3 did not work.  
 
# Technical Stuff

## Requirements
To be able to use this, you need to have [docker](https://www.docker.com/) installed. If
you are new to docker, I recommend following [this tutorial from Matthew Feickert](https://matthewfeickert.github.io/intro-to-docker/).

### Mac OSX

Download Docker for MacOS - [instructions](https://docs.docker.com/docker-for-mac/install/).

Docker is a full development platform for creating containerized apps, and Docker Desktop 
for Mac is the best way to get started with Docker on a Mac. To download Docker Desktop 
for MacOS, head to [Docker Hub](https://hub.docker.com/editions/community/docker-ce-desktop-mac).

Please read the relevant information on these pages, it should take no more than 5 minutes.
Another common way to install packages on Mac OSX is via the 
[homebrew package manager](https://brew.sh/). In the case of docker, you can easily 
install docker by setting up homebrew and executing brew cask install docker.

### Linux

Here are directions for two of the more common Linux distributions : 
[Ubuntu](https://docs.docker.com/engine/install/ubuntu/), [CentOS](https://docs.docker.com/engine/install/centos/)

Note that downloading and using docker in linux may be a bit more challenging due to permissions.

### Windows

It is highly recommended that you DO NOT use Windows. Few individuals use this OS within 
the HEP community as most tools are designed for Unix-based systems. If you do have a 
Windows machine, consider making your computer a dual-boot machine - [Link to Directions](https://opensource.com/article/18/5/dual-boot-linux).

Download Docker for Windows - [instructions](https://docs.docker.com/docker-for-windows/install/).

Docker Desktop for Windows is the Community Edition (CE) of Docker for Microsoft Windows. 
To download Docker Desktop for Windows, head to [Docker Hub](https://hub.docker.com/editions/community/docker-ce-desktop-windows).

## Build the Image Yourself
If you want to try and build the image yourself, then you can clone this repo and run the
following command
```
docker build -t myTagName .
```