#############################################
# start from the base contur image provided by the UCL folks
#############################################
FROM huangjoanna/contur

#############################################
# perform everything as root user in the main /contur directory
#############################################
USER root
WORKDIR /LHC-DMWG

#############################################
# CMake is needed to install stuff
# FROM : https://gitlab.cern.ch/atlas-sit/docker/-/blob/master/centos7-atlasos/Dockerfile#L25
#############################################
RUN wget https://cmake.org/files/v3.14/cmake-3.14.6-Linux-x86_64.tar.gz && \
    tar -C /usr/local --strip-components=1 --no-same-owner -xvf cmake-3.14.6-Linux-x86_64.tar.gz && \
    rm -rf cmake-3.14.6-Linux-x86_64.tar.gz

#############################################
# Install ROOT from source
# FROM : https://root.cern.ch/content/release-61406
#############################################
RUN wget https://root.cern.ch/download/root_v6.14.06.Linux-fedora27-x86_64-gcc7.3.tar.gz && \
    tar -xzvf root_v6.14.06.Linux-fedora27-x86_64-gcc7.3.tar.gz && \
    rm -f root_v6.14.06.Linux-fedora27-x86_64-gcc7.3.tar.gz && \
    source /LHC-DMWG/root/bin/thisroot.sh

#############################################
# Upgrade the python version to python3 if you want
# FROM : https://tecadmin.net/install-python-3-7-on-centos/
#############################################
# RUN cd /usr/src && \
#     wget https://www.python.org/ftp/python/3.7.4/Python-3.7.4.tgz && \
#     tar xzf Python-3.7.4.tgz && \
#     cd Python-3.7.4 && \
#     ./configure --enable-optimizations && \
#     make altinstall && \
#     rm /usr/src/Python-3.7.4.tgz && \

#######################################
# install MadGraph
# FROM : https://cp3.irmp.ucl.ac.be/projects/madgraph/
#######################################
RUN wget https://launchpad.net/mg5amcnlo/2.0/2.7.x/+download/MG5_aMC_v2.7.2.tar.gz && \
    tar -xzf MG5_aMC_v2.7.2.tar.gz && \
    rm MG5_aMC_v2.7.2.tar.gz
    
#######################################
# need rsync and a few other things to install pythia on this MG version
# this is needed if you were to be installing anything from the madgraph interface
# RUN yum -y install rsync gcc openssl-devel bzip2-devel libffi-devel
#
# install pythia inside of madgraph
# RUN cd MG5_aMC_v2_6_6 && \
#     echo "install pythia8" >> install.dat && \
#     ./bin/mg5_aMC install.dat && \
#     rm install.dat
#######################################

# get the DMsimp model and put it within the madgraph image
RUN cd MG5_aMC_v2_7_2/models && \
    wget http://feynrules.irmp.ucl.ac.be/raw-attachment/wiki/DMsimp/DMsimp_s_spin1_v2.1.zip && \
    unzip DMsimp_s_spin1_v2.1.zip && \
    rm DMsimp_s_spin1_v2.1.zip
    
# get the t-channel model
RUN cd MG5_aMC_v2_7_2/models && \
    wget http://feynrules.irmp.ucl.ac.be/raw-attachment/wiki/DMsimpt/dmsimpt_v1.3.ufo.tgz && \
    tar -xvzf dmsimpt_v1.3.ufo.tgz && \
    rm dmsimpt_v1.3.ufo.tgz
    
#######################################
# install LHAPDF
# FROM : https://lhapdf.hepforge.org/install.html . Noting that you have to designate 
#        the specific X.Y version you want
#######################################
RUN wget https://lhapdf.hepforge.org/downloads/?f=LHAPDF-6.2.3.tar.gz -O LHAPDF-6.2.3.tar.gz && \
    tar xf LHAPDF-6.2.3.tar.gz && \
    cd LHAPDF-6.2.3 && \
    ./configure --prefix=/LHC-DMWG/LHAPDF-Install && \
    make && \
    make install && \
    rm /LHC-DMWG/LHAPDF-6.2.3.tar.gz 
    
# obtain the necessary PDF sets
RUN wget http://lhapdfsets.web.cern.ch/lhapdfsets/current/CT10nlo.tar.gz -O- | tar xz -C /LHC-DMWG/LHAPDF-Install/share/LHAPDF

#######################################
# install pythia8.3 standalone
# FROM : INSTALL file found in the tarball
#######################################
RUN wget http://home.thep.lu.se/~torbjorn/pythia8/pythia83.tgz && \
    tar xzvf pythia83.tgz && \
    cd pythia8301 && \
    ./configure && \
    make && \
    rm /LHC-DMWG/pythia83.tgz

#######################################
# install hepmc
# FROM : http://hepmc.web.cern.ch/hepmc/building.html
# NOTE : This is not using HepMC3 because the cmake build did not work
#######################################
# RUN wget http://hepmc.web.cern.ch/hepmc/releases/HepMC3-3.2.1.tar.gz && \
#     tar xzvf HepMC3-3.2.1.tar.gz  && \
#     cd HepMC3-3.2.1 && \
#     mkdir build && \
#     cd build && \
#     cmake -DCMAKE_INSTALL_PREFIX=/root/HepMC -DHEPMC3_BUILD_EXAMPLES=OFF -DPYTHIA8_ROOT_DIR=/LHC-DMWG/pythia8301 -DROOT_DIR=/LHC-DMWG/root/bin ../. && \
#     make 

RUN wget http://hepmc.web.cern.ch/hepmc/releases/hepmc2.06.10.tgz && \
    tar -xzvf hepmc2.06.10.tgz && \
    cd HepMC-2.06.10/ && \
    mkdir ../HepMC-Install && \
    ./bootstrap && \
    ./configure --prefix=/LHC-DMWG/HepMC-Install --with-momentum=GEV --with-length=MM && \
    make && \
    make install && \
    rm ../hepmc2.06.10.tgz

#######################################
# leave the image in the contur directory space
#######################################
WORKDIR /LHC-DMWG

COPY setup.sh .
RUN chmod 777 setup.sh
CMD source /LHC-DMWG/setup.sh
  